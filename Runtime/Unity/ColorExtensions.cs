﻿using UnityEngine;

namespace DungeonSoft.HelpfulScripts.Unity {
	public static class ColorExtensions {

		public static Color Alpha0(this Color color) {
			return color.Alpha(0);
		}

		public static Color Alpha(this Color color, float alpha) {
			color.a = alpha;
			return color;
		}

		public static Color SetAllColorsButAlpha(this Color color, float mod) {
			color.r *= mod;
			color.g *= mod;
			color.b *= mod;
			return color;
		}

	}
}
