﻿using System.Collections.Generic;

namespace DungeonSoft.HelpfulScripts {
	public abstract class LongDelta<T> {
		protected LongDelta(float maxTime) {
			this.maxTime = maxTime;
		}

		private readonly float maxTime;

		private class Entry {
			public double addedTime;
			public T t;
			public Entry next;
		}

		private Entry first, last;

		public void AddValue(double currentTime, T t) {
			var entry = new Entry { addedTime = currentTime, t = t };
			if (first == null) first = entry;
			else {
				if (last != null) {
					last.next = entry;
				}
			}

			last = entry;
		}

		public T GetLongDelta(double currentTime) {
			// Traverse tree
			while (first != null) {
				if (first.addedTime >= currentTime - maxTime) break;
				first = first.next;
			}

			if (first == null) {
				//Debug.LogWarning("No entries available to get long delta from!");
				return default(T);
			}

			var list = new List<T>();
			Entry current = first;
			while (current != null) {
				list.Add(current.t);
				current = current.next;
			}

			return GetDelta(list);

		}

		protected abstract T GetDelta(IList<T> values);

	}

}
