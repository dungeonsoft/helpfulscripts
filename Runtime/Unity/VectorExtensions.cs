﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonSoft.HelpfulScripts.Unity {
	public static class VectorExtensions {
		public static Vector2 ToVector2(this Vector2Int v) {
			return new Vector2(v.x, v.y);
		}

		public static Vector2Int ToVector2Int(this Vector2 v) {
			return new Vector2Int(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y));
		}

		//public static Vector2 ToVector2(this Vector2Int v)

		public static bool IsZero(this Vector2 v) {
			return new Vector3(v.x, v.y, 0).IsZero();
		}

		public static bool IsZero(this Vector3 v) {
			return Mathf.Approximately(v.x, 0) &&
				   Mathf.Approximately(v.y, 0) &&
				   Mathf.Approximately(v.z, 0);
		}


		// ReSharper disable once InconsistentNaming
		public static Vector2 ToVector2XZ(this Vector3 v) {
			return new Vector2(v.x, v.z);
		}

		// ReSharper disable once InconsistentNaming
		public static Vector3 ToVector3XZ(this Vector2 v) {
			return new Vector3(v.x, 0, v.y);
		}

		public static Vector3 ToVector3XZ(this Vector2 v, float y) {
			return new Vector3(v.x, y, v.y);
		}


		public static Vector3 FlipXZ(this Vector3 v) {
			return new Vector3(v.z, v.y, v.x);
		}

		public static Vector2 ToVector2XY(this Vector3 v) {
			return new Vector2(v.x, v.y);
		}

		public static Vector2 ToVector2YZ(this Vector3 v) {
			return new Vector2(v.y, v.z);
		}

		public static Vector2 Rotate(this Vector2 v, float degrees) {
			float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
			float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

			float tx = v.x;
			float ty = v.y;
			v.x = (cos * tx) - (sin * ty);
			v.y = (sin * tx) + (cos * ty);
			return v;
		}

		public static float SignedAngle(this Vector2 a, Vector2 b) {
			float ang = Vector2.Angle(a, b);
			return Vector3.Cross(a, b).z > 0 ? -ang : ang;

		}

		public static float DistanceToLine(this Vector2 point, Vector2 lineStart, Vector2 lineEnd) {
			Vector2 a = lineStart;
			Vector2 b = lineEnd;
			Vector2 p = point;

			Vector2 m = b - a; // Direction vector
			float t0 = Vector2.Dot(m, p - a) / Vector2.Dot(m, m);
			Vector2 intersect = a + t0 * m;
			Vector2 c;
			if (t0 < 0) c = a;
			else if (t0 > 1) c = b;
			else c = intersect;

			return Vector2.Distance(p, c);

			// Mathf.Sin(Mathf.Atan2(b.y - a.y, b.x - a.x) - Mathf.Atan2(p.y - a.y, p.x - a.x)) * Mathf.Sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
		}

		public static bool PointInsideTriangle(this Vector2 point, Vector2 triA, Vector2 triB, Vector2 triC) {
			Vector2 s = point;
			/*
			Vector2 triA = cornerVertices[0].ToWorldSpace2();
			Vector2 triB= cornerVertices[1].ToWorldSpace2();
			Vector2 triC= cornerVertices[2].ToWorldSpace2();
			*/

			// Magic follows:
			float asX = s.x - triA.x;
			float asY = s.y - triA.y;

			bool sAb = (triB.x - triA.x) * asY - (triB.y - triA.y) * asX > 0;

			if ((triC.x - triA.x) * asY - (triC.y - triA.y) * asX > 0 == sAb) return false;

			return (triC.x - triB.x) * (s.y - triB.y) - (triC.y - triB.y) * (s.x - triB.x) > 0 == sAb;
		}

		public static bool PointInsideTriangle(this Vector2 point, IList<Vector2> points) {
			if (points.Count != 3) throw new ArgumentException("Points count is not 3!");
			return point.PointInsideTriangle(points[0], points[1], points[2]);
		}

		public static float Distance(this Vector2 point1, Vector2 point2) {
			return Vector2.Distance(point1, point2);
		}

		public static float Distance(this Vector2Int point1, Vector2Int point2) {
			return Vector2Int.Distance(point1, point2);
		}

		public static Vector2Int FlipX(this Vector2Int p) {
			return new Vector2Int(-p.x, p.y);
		}


		public static Vector2Int FlipY(this Vector2Int p) {
			return new Vector2Int(p.x, -p.y);
		}

		public static Vector2Int ToVector(this long[] array) {
			if (array.Length != 2) throw new Exception("Long array not length of 2!");
			return new Vector2Int((int)array[0], (int)array[1]);
		}

		public static Vector2Int ClampToBounds(this Vector2Int v, RectInt area) {
			v.Clamp(area.min, area.max);
			return v;
			/*
			v.x = Mathf.Clamp(v.x, area.min.x, area.max.x);
			v.y = Mathf.Clamp(v.y, area.min.y, area.min.y);
			return v;
		*/
		}
		/*
		public static int GetMD5(this Vector2Int v) {
			using MD5 md5 = MD5.Create();
			var inputBytes = Encoding.ASCII.GetBytes(v.ToString());
			var hashBytes = md5.ComputeHash(inputBytes);
			return Convert.ToBase64String(hashBytes).GetHashCode();
		}*/


	}
}