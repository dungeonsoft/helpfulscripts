﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DungeonSoft.HelpfulScripts {
	public interface ICache {
		int Size { get; }
		int SizeLimit { get; set; }
		float SizePercentage => 1.0f * Size / SizeLimit;
	}

	public class Cache<TKey, TValue, TTime> : ICache {

		private Dictionary<TKey, TValue> current;
		private Dictionary<TKey, TValue> previous;

		private TTime currentCacheValidUntil = default;

		//private readonly int sizeLimit;
		public int SizeLimit { get; set; }
		private readonly TTime slidingExpiration;
		private readonly Func<TTime> getCurrentTime;
		private readonly Func<TTime, TTime, bool> isLeftLargerThanRight;
		private readonly Func<TTime, TTime, TTime> addition;

		public Cache(int sizeLimit, TTime slidingExpiration, Func<TTime> getCurrentTime, Func<TTime, TTime, bool> isLeftLargerThanRight, Func<TTime, TTime, TTime> addition) {
			SizeLimit = sizeLimit;
			this.slidingExpiration = slidingExpiration;
			this.getCurrentTime = getCurrentTime;
			this.isLeftLargerThanRight = isLeftLargerThanRight;
			this.addition = addition;

			CheckValidity();

			/*
			int currentFrame = RealmsBlitGame.FrameCount;
			current = new Dictionary<TKey, TValue>();
			currentCacheValidUntil = currentFrame + slidingExpiration;
			*/
		}

		private void CheckValidity() {

			TTime currentFrame = getCurrentTime(); //RealmsBlitGame.FrameCount;
			if (current != null && isLeftLargerThanRight(currentCacheValidUntil, currentFrame)) return; // All is ok!

			previous = current;
			current = new Dictionary<TKey, TValue>();
			currentCacheValidUntil = addition(currentFrame, slidingExpiration);
		}

		// = (long)Mathf.Pow(2, 16);
		/*
		private readonly MemoryCache memoryCache = new MemoryCache(new MemoryCacheOptions() {
			TrackStatistics = true,
			SizeLimit = SizeLimit
		}); */

		public int Size {
			get {
				var size = 0;
				if (current != null) size += current.Count;
				if (previous != null) size += previous.Count;
				return size;
				/*
				MemoryCacheStatistics stats = memoryCache.GetCurrentStatistics();

				if (stats != null && stats.CurrentEstimatedSize == null) return 0;
				return (long)stats.CurrentEstimatedSize;
				*/
			}
		}

		//public float SizePercentage => (float)(1.0 * Size / SizeLimit);

		public virtual bool TryGet(TKey key, out TValue value) {
			//if (CachingDisabled) return null;
			value = default;
			if (key == null) return false;

			CheckValidity();

			if (previous != null) {
				if (previous.TryGetValue(key, out value)) {
					previous.Remove(key);
					current.Add(key, value);
					return true;
				}
			}

			if (current.TryGetValue(key, out value)) return true;

			return false;

			//return memoryCache.Get(key.ToString()) as TValue;
		}

		public virtual TValue Get(TKey key) {
			if (TryGet(key, out TValue value)) return value;
			return default;
		}

		//private readonly MemoryCacheEntryOptions cacheEntryOptions = new MemoryCacheEntryOptions().SetSize(1).SetSlidingExpiration(TimeSpan.FromMinutes(10));
		// = RealmsBlitGame.FPS * 60 * 5; // 5 minutes

		public virtual void Set(TKey key, TValue value) {
			//if (cachingDisabled) return;
			if (key == null) return;

			CheckValidity();

			if (previous != null) {
				if (previous.TryGetValue(key, out var v)) {
					previous.Remove(key);
				}
			}

			if (Size > SizeLimit) {
				// Remove something
				if (previous == null || previous.Count == 0) current.Remove(current.First().Key);
				else previous.Remove(previous.First().Key);
			}

			if (current.TryAdd(key, value)) return;

			current[key] = value;

			//memoryCache.Set(key.ToString(), value, cacheEntryOptions);
		}

		public void Clear() {
			currentCacheValidUntil = default;
			current = null;
			previous = null;
			//previous?.Clear();
		}

	}
}
