﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DungeonSoft.HelpfulScripts {
	public class DungeonSoftEntity<T> where T : IDungeonSoftComponent {

		// COMPONENTS:
		[Obsolete]
		private readonly LinkedList<T> _components = new();

		public virtual IEnumerable<T> GetComponents() {
			return _components.SafeIterate();
			//return components;
		}
		/*
		public virtual object GetComponent(Type type) {

		}
		*/
		public virtual T1 GetComponent<T1>() where T1 : class, T {
			return GetComponents<T1>().FirstOrDefault();
		}

		public virtual IEnumerable<T1> GetComponents<T1>() where T1 : class, T {
			return GetComponents().OfType<T1>();
		}

		public virtual object AddComponent(Type type) {
			object t = Activator.CreateInstance(type);
			if (t is not T) return null;
			_components.AddLast((T)t);
			return t;
		}

		public T1 AddComponent<T1>() where T1 : class, T, new() {
			return (T1)AddComponent(typeof(T1));
		}
		/*
		public virtual bool RemoveComponent<T1>() where T1 : class, T {
			return components.Remove(GetComponent<T1>());
		}*/

		
		public T1 GetOrAddComponent<T1>() where T1 : class, T, new() {
			T1 t = GetComponent<T1>() ?? AddComponent<T1>();
			return t;
		}

	}
	public interface IDungeonSoftComponent { }

}
