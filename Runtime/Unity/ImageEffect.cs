﻿using UnityEngine;

namespace DungeonSoft.HelpfulScripts.Unity {
	/// <summary>
	/// Used for drawing a shader/material straight into the camera
	/// </summary>
	public sealed class ImageEffect : MonoBehaviour {

		public Shader shader;

		public Material mat;

		// ReSharper disable once UnusedMember.Local
		private void Start() {
			if (mat != null) return;
			if (shader == null) return;
			mat = new Material(shader);
		}

		// ReSharper disable once UnusedMember.Local
		// ReSharper disable once SuggestBaseTypeForParameter
		private void OnRenderImage(RenderTexture src, RenderTexture dest) {
			Graphics.Blit(src, dest, mat);
		}

	}

}