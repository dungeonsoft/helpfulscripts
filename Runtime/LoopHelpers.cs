﻿using System;

namespace DungeonSoft.HelpfulScripts {
	public static class LoopHelpers {

		public static void For(int max, Action<int> action) {
			For(0, max, action);
		}

		public static void For(int start, int max, Action<int> action) {
			For2D(start, max, 0, 1, (i, j) => action(i));
		}

		public static void For2D(int maxI, int maxJ, Action<int, int> action) {
			For2D(0, maxI, 0, maxJ, action);
		}

		public static void For2D(int startI, int maxI, int startJ, int maxJ, Action<int, int> action) {
			for (int i = startI; i < maxI; i++)
				for (int j = startJ; j < maxJ; j++)
					action(i, j);
		}

	}
}
