﻿namespace DungeonSoft.HelpfulScripts {
	public struct StringInvariant {

		private string value;

		public static implicit operator string(StringInvariant s) {
			return s.value;
		}

		public static implicit operator StringInvariant(string s) {
			return new StringInvariant { value = s.ToLowerInvariant() };
		}

		public override string ToString() {
			return value;
		}

		public override int GetHashCode() {
			return value.GetHashCode();
		}
	}
}
