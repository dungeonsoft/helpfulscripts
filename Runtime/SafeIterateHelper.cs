﻿using System.Collections.Generic;

namespace DungeonSoft.HelpfulScripts {
	public static class SafeIterateHelper {

		public static IEnumerable<T> SafeIterate<T>(this LinkedList<T> list) {
			if (list.Count == 0) yield break;
			LinkedListNode<T> e = list.First;
			while (e != null) {
				yield return e.Value;
				e = e.Next;
			}
		}

	}
}
