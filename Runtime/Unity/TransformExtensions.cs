﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DungeonSoft.HelpfulScripts.Unity {
	public static class TransformExtensions {
		public static IEnumerable<Transform> GetChildren(this Transform transform) {
			for (var i = 0; i < transform.childCount; i++) {
				foreach (Transform c in transform.GetChild(i)) yield return c;
			}
		}

		public static IEnumerable<Transform> GetChildrenRecursive(this Transform transform, bool activeOnly) {
			if (activeOnly && !transform.gameObject.activeInHierarchy) yield break;

			yield return transform;
			foreach (Transform c in transform.GetChildren().SelectMany(x => x.GetChildrenRecursive(activeOnly))) yield return c;
		}

		public static IEnumerable<GameObject> GetChildrenRecursive(this GameObject gameObject, bool activeOnly) {
			return gameObject.transform.GetChildrenRecursive(activeOnly).Select(x => x.gameObject);
		}
	}
}
