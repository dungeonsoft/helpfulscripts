﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DungeonSoft.HelpfulScripts.Unity {
	public sealed class ObjectPool {

		private readonly GameObject prefab;

		public ObjectPool([NotNull] GameObject prefab, int preload = 0) {
			if (prefab == null) throw new NullReferenceException("Prefab cannot be null!");
			this.prefab = prefab;
			for (var i = 0; i < preload; i++) Despawn(Spawn());
		}

		private readonly Queue<GameObject> pool = new Queue<GameObject>();

		public GameObject Spawn() {
			GameObject obj = pool.Count == 0 ? Object.Instantiate(prefab) : pool.Dequeue();
			obj.name = prefab.name;
			obj.SetActive(true);
			return obj;
		}

		public void Despawn([NotNull] GameObject target) {
			if (target == null) throw new NullReferenceException("Cannot despawn a null gameobject!");
			target.SetActive(false);
			pool.Enqueue(target);
		}

		public void Clear() {
			while (pool.Count > 0) Object.Destroy(pool.Dequeue());
			pool.Clear();
		}

	}
}
