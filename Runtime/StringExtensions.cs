﻿using System.IO;

namespace DungeonSoft.HelpfulScripts {
	public static class StringExtensions {
		public static Stream GenerateStream(this string s) {
			var stream = new MemoryStream();
			var writer = new StreamWriter(stream);
			writer.Write(s);
			writer.Flush();
			stream.Position = 0;
			return stream;
		}

		public static Stream GenerateStream(this byte[] bytes) {
			var stream = new MemoryStream();
			var writer = new StreamWriter(stream);
			writer.Write(bytes);
			writer.Flush();
			stream.Position = 0;
			return stream;
		}

	}
}
