﻿using UnityEngine;

namespace DungeonSoft.HelpfulScripts.Unity {
	public class KeyCurveVector2 : KeyCurve<Vector2> {
		protected override Vector2 Lerp(Vector2 a, Vector2 b, float t) {
			return Vector2.Lerp(a, b, t);
		}
	}

	public class KeyCurveVector3 : KeyCurve<Vector3> {
		protected override Vector3 Lerp(Vector3 a, Vector3 b, float t) {
			return Vector3.Lerp(a, b, t);
		}
	}

	public class KeyCurveColor : KeyCurve<Color> {
		protected override Color Lerp(Color a, Color b, float t) {
			return Color.Lerp(a, b, t);
		}
	}
}
