﻿using System;

namespace AssemblyCSharp.Assets {
	public class MathInt {
		public static int PingPong(int input, int min, int max) {
			return Oscillate(input, min, max);
		}

		public static int Oscillate(int input, int min, int max) {
			int range = max - min;
			return min + Math.Abs(((input + range) % (range * 2)) - range);
		}



	}
}
