﻿namespace DungeonSoft.HelpfulScripts {
	public static class PyreNoise {
		public static float Range(float min, float max, ref int seed) {
			return min + DaveNoise.Noise3D(seed + 24, ++seed - 266, ++seed) * (max - min);
		}

		public static float Range(float min, float max, int seed) {
			return Range(min, max, ref seed);
		}

		public static int Range(int min, int max, ref int seed) {
			return (int)Range(1f * min, max, ref seed);
		}

		public static int Range(int min, int max, int seed) {
			return Range(min, max, ref seed);
		}
		/*
		public static Vector2 RandomInsideUnitCircle(ref int seed) {
			return DungeonSoft.InputHelper.RoundVector(new Vector2(Range(-1f, 1f, ref seed), Range(-1f, 1f, ref seed)));
		}

		public static Vector2 RandomInsideUnitCircle(int seed) {
			return RandomInsideUnitCircle(ref seed);
		}*/

		public static float Simplex(float x, float y, float frequency, float min, float max, ref int seed) {
			int s = seed % 50000; // Simplex goes haywire if seed is a large!

			float t1 = (1 + DaveNoise.SimplexNoise3D(13535 + s++, 25033 + x + s++, 23423 + y + s++, frequency, 1)) *
					   0.5f;
			seed = s;
			float t2 = (-0.01f) * t1 + (1.01f) * (1 - t1); // Hack, koska vitun davenoise
			return min * t2 + max * (1 - t2); // Basic Lerp, en käyttäny unityn mathf.lerp koska ei pakolla thread safe
		}

		public static float Simplex(float x, float y, float frequency, float min, float max, int seed) {
			return Simplex(x, y, frequency, min, max, ref seed);
		}

	}
}