# HelpfulScripts

Contains useful C#-code for Unity projects. Downloadable as unity package:

```
https://gitlab.com/dungeonsoft/helpfulscripts.git
```