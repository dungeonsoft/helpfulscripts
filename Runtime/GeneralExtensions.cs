﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions; //using UnityEngine;
									  //using Object = UnityEngine.Object;

namespace DungeonSoft.HelpfulScripts {
	public static class GeneralExtensions {


		/*
		public static void AddActions(this Button button, params UnityAction[] actions) {
			foreach (UnityAction a in actions) {
				UnityAction a1 = a;
				button.onClick.AddListener(a1);
			}
		}
		*/

		public static string SplitCamelCase(this string str) {
			return Regex.Replace(
				Regex.Replace(
					str,
					@"(\P{Ll})(\P{Ll}\p{Ll})",
					"$1 $2"
				),
				@"(\p{Ll})(\P{Ll})",
				"$1 $2"
			);
		}

		public static string UppercaseFirst(this string s) {
			// Check for empty string.
			if (string.IsNullOrEmpty(s)) {
				return s;
			}

			// Return char and concat substring.
			return char.ToUpper(s[0]) + s.Substring(1);
		}

		/// <summary>
		/// Uppercase if true
		/// </summary>
		public static string UppercaseFirst(this string s, bool uppercase) {
			return !uppercase ? s : s.UppercaseFirst();
		}

		public static IEnumerable<T> GetCustomAttributes<T>(this MemberInfo info, bool inherit = true) where T : Attribute {
			return info.GetCustomAttributes(typeof(T), inherit).Cast<T>();
		}

		public static IEnumerable<T> GetCustomAttributes<T>(this MethodInfo info, bool inherit = true) where T : Attribute {
			return info.GetCustomAttributes(typeof(T), inherit).Cast<T>();
		}

		public static IEnumerable<T> GetCustomAttributes<T>(this FieldInfo info, bool inherit = true) where T : Attribute {
			return info.GetCustomAttributes(typeof(T), inherit).Cast<T>();
		}

		public static IEnumerable<Type> GetTypesThatInheritFromAllAssemblies(this Type type, bool allowObsolete = false) {
			return AppDomain.CurrentDomain.GetAssemblies().SelectMany(assembly => GetTypesThatInheritFrom(type, assembly, allowObsolete));
		}

		public static IEnumerable<Type> GetTypesThatInheritFrom(this Type type, Assembly assembly, bool allowObsolete = false) {
			IOrderedEnumerable<Type> values = assembly.GetTypes().Where(x => x.IsClass && !x.IsAbstract && (x.IsSubclassOf(type) || type.IsAssignableFrom(x)))
				.OrderBy(y => y.Name);
			foreach (Type v in values) {
				if (!allowObsolete && v.GetCustomAttributes<ObsoleteAttribute>().Any()) continue;
				yield return v;
			}



		}






		public static string PercentageToString(this float percentage, int decimals) {
			return (100f * percentage).ToString("F" + decimals) + "%";
		}

		public static void InvokeAll(this IEnumerable<Action> actions) {
			foreach (Action action in actions) {
				action();
			}
		}

		// Siirrä tää jonnekkin järkevään paikkaan
		public static double InvokeAndGetElapsed(this Action action) {
			Stopwatch sw = Stopwatch.StartNew();
			action();
			return sw.Elapsed.TotalSeconds;
		}

		public static int RoundToInt(this float f) {
			return Convert.ToInt32(f);
		}


		public static string AsPath(this string s) {
			var list = new List<string>();
			foreach (string ss in s.Split('/', '\\')) {
				if (ss != "..") list.Add(ss);
				else {
					if (list.Count == 0) throw new Exception("Invalid path: " + s);
					list.RemoveAt(list.Count - 1);
				}
			}

			s = "";
			for (var i = 0; i < list.Count; i++) {
				s += list[i];
				if (i != list.Count - 1) s += "/";
			}

			return s;
		}

	}
}