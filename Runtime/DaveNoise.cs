﻿using System;

namespace DungeonSoft.HelpfulScripts {
	public static class DaveNoise {

		public static float Noise1D(int x) {
			x = (x << 13) ^ x;
			return (float)((1.0 - ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0) + 1) * 0.5f;
		}

		public static float Noise2D(int x, int y) {
			int n = x + y * 57;
			n = (n << 13) ^ n;
			return (float)((1.0 - ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0) + 1) * 0.5f;
		}

		public static float Noise3D(int x, int y, int z) {
			return Noise1D(x * 1663 + y * 1667 + z * 1669);
		}

		public static float SmoothNoise1D(int x) {
			return Noise1D(x) / 2 + Noise1D(x - 1) / 4 + Noise1D(x + 1) / 4;
		}

		public static float SmoothNoise2D(int x, int y) {
			float corners = (Noise2D(x - 1, y - 1) + Noise2D(x + 1, y - 1) + Noise2D(x - 1, y + 1) + Noise2D(x + 1, y + 1)) / 16;
			float sides = (Noise2D(x - 1, y) + Noise2D(x + 1, y) + Noise2D(x, y - 1) + Noise2D(x, y + 1)) / 8;
			float center = Noise2D(x, y) / 4;

			return corners + sides + center;
		}

		private static float LinearInterpolation1D(float a, float b, float x) {
			return a * (1 - x) + b * x;
		}

		private static float CosineInterpolation1D(float a, float b, float x) {
			double ft = x * 3.1415927;
			double f = (1 - Math.Cos(ft)) * .5;

			return (float)(a * (1 - f) + b * f);
		}

		private static float CubicInterpolation1D(float v0, float v1, float v2, float v3, float x) {
			float P = (v3 - v2) - (v0 - v1);
			float Q = (v0 - v1) - P;
			float R = v2 - v0;
			float S = v1;

			return P * x * x * x + Q * x * x + R * x + S;
		}

		private static float LinearInterpolation2D(float v00, float v01, float v10, float v11, float x, float y) {
			float x0 = LinearInterpolation1D(v00, v01, y);
			float x1 = LinearInterpolation1D(v10, v11, y);
			return LinearInterpolation1D(x0, x1, x);
		}

		private static float CosineInterpolation2D(float v00, float v01, float v10, float v11, float x, float y) {
			float x0 = CosineInterpolation1D(v00, v01, y);
			float x1 = CosineInterpolation1D(v10, v11, y);
			return CosineInterpolation1D(x0, x1, x);
		}

		private static float CubicInterpolation2D(float v_12, float v02, float v12, float v22, float v_11, float v01, float v11, float v21, float v_10, float v00, float v10, float v20, float v_1_1, float v0_1, float v1_1, float v2_1, float x, float y) {
			float x_1 = CubicInterpolation1D(v_1_1, v_10, v_11, v_12, y);
			float x0 = CubicInterpolation1D(v0_1, v00, v01, v02, y);
			float x1 = CubicInterpolation1D(v1_1, v10, v11, v12, y);
			float x2 = CubicInterpolation1D(v2_1, v20, v21, v22, y);
			return CubicInterpolation1D(x_1, x0, x1, x2, x);
		}

		private static float LinearInterpolation3D(float v000, float v010, float v100, float v110, float v001, float v011, float v101, float v111, float x, float y, float z) {
			float x0 = LinearInterpolation1D(v000, v010, y);    // Einfluss von v00 und v10 auf die Ebene x0 
			float x1 = LinearInterpolation1D(v100, v110, y);    // Einfluss von v01 und v11 auf die Ebene x0 
			float x2 = LinearInterpolation1D(v001, v011, y);    // Einfluss von v00 und v10 auf die Ebene x0 
			float x3 = LinearInterpolation1D(v101, v111, y);    // Einfluss von v01 und v11 auf die Ebene x0 

			float y0 = LinearInterpolation1D(x0, x1, x);        // Interpolation der x0 und x1 Ebene 
			float y1 = LinearInterpolation1D(x2, x3, x);        // Interpolation der x2 und x3 Ebene 

			float result = LinearInterpolation1D(y0, y1, z);    // Interpolation der y0 und y1 Ebene 

			return result;
		}

		private static float CosineInterpolation3D(float v000, float v010, float v100, float v110, float v001, float v011, float v101, float v111, float x, float y, float z) {
			float x0 = CosineInterpolation1D(v000, v010, y);    // Einfluss von v00 und v10 auf die Ebene x0 
			float x1 = CosineInterpolation1D(v100, v110, y);    // Einfluss von v01 und v11 auf die Ebene x0 
			float x2 = CosineInterpolation1D(v001, v011, y);    // Einfluss von v00 und v10 auf die Ebene x0 
			float x3 = CosineInterpolation1D(v101, v111, y);    // Einfluss von v01 und v11 auf die Ebene x0 

			float y0 = CosineInterpolation1D(x0, x1, x);        // Interpolation der x0 und x1 Ebene 
			float y1 = CosineInterpolation1D(x2, x3, x);        // Interpolation der x2 und x3 Ebene 

			float result = CosineInterpolation1D(y0, y1, z);    // Interpolation der y0 und y1 Ebene 

			return result;
		}

		private static float CubicInterpolation3D(float[][][] values, float x, float y, float z) {
			float xy_1_1 = CubicInterpolation1D(values[0][0][0], values[0][0][1], values[0][0][2], values[0][0][3], z); // Einfluss auf die x-1y-1-Ebene 
			float xy_10 = CubicInterpolation1D(values[0][1][0], values[0][1][1], values[0][1][2], values[0][1][3], z);  // Einfluss auf die x-1y0-Ebene 
			float xy_11 = CubicInterpolation1D(values[0][2][0], values[0][2][1], values[0][2][2], values[0][2][3], z);  // Einfluss auf die x-1y1-Ebene 
			float xy_12 = CubicInterpolation1D(values[0][3][0], values[0][3][1], values[0][3][2], values[0][3][3], z);  // Einfluss auf die x-1y2-Ebene 
			float xy0_1 = CubicInterpolation1D(values[1][0][0], values[1][0][1], values[1][0][2], values[1][0][3], z);  // Einfluss auf die x0y-1-Ebene 
			float xy00 = CubicInterpolation1D(values[1][1][0], values[1][1][1], values[1][0][2], values[1][1][3], z);   // Einfluss auf die x0y0-Ebene 
			float xy01 = CubicInterpolation1D(values[1][2][0], values[1][2][1], values[1][0][2], values[1][2][3], z);   // Einfluss auf die x0y1-Ebene 
			float xy02 = CubicInterpolation1D(values[1][3][0], values[1][3][1], values[1][0][2], values[1][3][3], z);   // Einfluss auf die x0y2-Ebene 
			float xy1_1 = CubicInterpolation1D(values[2][0][0], values[2][0][1], values[2][0][2], values[2][0][3], z);  // Einfluss auf die x1y-1-Ebene 
			float xy10 = CubicInterpolation1D(values[2][1][0], values[2][1][1], values[2][0][2], values[2][1][3], z);   // Einfluss auf die x1y0-Ebene 
			float xy11 = CubicInterpolation1D(values[2][2][0], values[2][2][1], values[2][0][2], values[2][2][3], z);   // Einfluss auf die x1y1-Ebene 
			float xy12 = CubicInterpolation1D(values[2][3][0], values[2][3][1], values[2][0][2], values[2][3][3], z);   // Einfluss auf die x1y2-Ebene 
			float xy2_1 = CubicInterpolation1D(values[3][0][0], values[3][0][1], values[3][0][2], values[3][0][3], z);  // Einfluss auf die x2y-1-Ebene 
			float xy20 = CubicInterpolation1D(values[3][1][0], values[3][1][1], values[3][0][2], values[3][1][3], z);   // Einfluss auf die x2y0-Ebene 
			float xy21 = CubicInterpolation1D(values[3][2][0], values[3][2][1], values[3][0][2], values[3][2][3], z);   // Einfluss auf die x2y1-Ebene 
			float xy22 = CubicInterpolation1D(values[3][3][0], values[3][3][1], values[3][0][2], values[3][3][3], z);   // Einfluss auf die x2y2-Ebene 
			float x_1 = CubicInterpolation1D(xy_1_1, xy_10, xy_11, xy_12, y);                                           // Einfluss auf die x-1 Ebene 
			float x0 = CubicInterpolation1D(xy0_1, xy00, xy01, xy02, y);                                                // Einfluss auf die x0 Ebene 
			float x1 = CubicInterpolation1D(xy1_1, xy10, xy11, xy12, y);                                                // Einfluss auf die x1 Ebene 
			float x2 = CubicInterpolation1D(xy2_1, xy20, xy21, xy22, y);                                                // Einfluss auf die x2 Ebene 
			float result = CubicInterpolation1D(x_1, x0, x1, x2, x);                                                    // Interpolaton der Einflüsse 

			return result;
		}

		/// <summary>
		/// Linear Interpolated 1D Value Noise
		/// </summary>
		/// <param name="x"></param>
		/// <param name="frequency"></param>
		/// <param name="amplitude"></param>
		/// <param name="persistance"></param>
		/// <param name="octaves"></param>
		/// <returns></returns>
		public static float ValueNoise1DLinear(float x, float frequency, float amplitude, float persistence, int octaves) {

			float value = 0;
			float pointPrev;
			float pointAfter;
			int xFrqz;

			frequency = 1 / frequency;

			for (int i = 0; i < octaves; i++) {

				xFrqz = FastFloor(x * frequency);
				pointPrev = Noise1D(xFrqz);
				pointAfter = Noise1D(xFrqz + 1);
				value += LinearInterpolation1D(pointPrev, pointAfter, x % (1 / frequency) * frequency) * amplitude;

				frequency *= 2;
				amplitude *= persistence;

			}
			return value;

		}

		/// <summary>
		/// Cosine Interpolated 1D Value Noise
		/// </summary>
		/// <param name="x"></param>
		/// <param name="frequency"></param>
		/// <param name="amplitude"></param>
		/// <param name="persistance"></param>
		/// <param name="octaves"></param>
		/// <returns></returns>
		public static float ValueNoise1DCosine(int x, float frequency, float amplitude, float persistence, float octaves) {
			float value = 0;
			frequency = 1 / frequency;
			float PointPrev = 0;
			float PointAfter = 0;
			for (int i = 0; i < octaves; i++) {
				PointPrev = Noise1D((int)(x * frequency));
				PointAfter = Noise1D((int)(x * frequency) + 1);
				value += CosineInterpolation1D(PointPrev, PointAfter, x % (1 / frequency) * frequency) * amplitude; // Modulo (1 / frequency), um zu gewährleisten, dass x zwischen 0 und 1 liegt 
				frequency *= 2;
				amplitude *= persistence;
			}
			return value;
		}

		/// <summary>
		/// Cubic Interpolated 1D Value Noise
		/// </summary>
		/// <param name="x"></param>
		/// <param name="frequency"></param>
		/// <param name="amplitude"></param>
		/// <param name="persistance"></param>
		/// <param name="octaves"></param>
		/// <returns></returns>
		public static float ValueNoise1DCubic(int x, float frequency, float amplitude, float persistence, float octaves) {
			float value = 0;
			frequency = 1 / frequency;
			float PointPrev2 = 0;
			float PointPrev1 = 0;
			float PointAfter1 = 0;
			float PointAfter2 = 0;
			for (int i = 0; i < octaves; i++) {
				PointPrev2 = Noise1D((int)(x * frequency) - 1);
				PointPrev1 = Noise1D((int)(x * frequency));
				PointAfter1 = Noise1D((int)(x * frequency) + 1);
				PointAfter2 = Noise1D((int)(x * frequency) + 2);
				value += CubicInterpolation1D(PointPrev2, PointPrev1, PointAfter1, PointAfter2, x % (1 / frequency) * frequency) * amplitude;
				frequency *= 2;
				amplitude *= persistence;
			}
			return value;
		}

		/// <summary>
		/// Linear Interpolated 2D Value Noise
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="frequency"></param>
		/// <param name="amplitude"></param>
		/// <param name="persistance"></param>
		/// <param name="octaves"></param>
		/// <returns></returns>
		public static float ValueNoise2DLinear(float x, float y, float frequency, float amplitude, float persistence, int octaves) {

			float value = 0;
			float v00, v01, v10, v11;
			int xFrqz, yFrqz;

			frequency = 1 / frequency;

			for (int i = 0; i < octaves; i++) {

				xFrqz = FastFloor(x * frequency);
				yFrqz = FastFloor(y * frequency);
				v00 = Noise2D(xFrqz, yFrqz);
				v01 = Noise2D(xFrqz, yFrqz + 1);
				v10 = Noise2D(xFrqz + 1, yFrqz);
				v11 = Noise2D(xFrqz + 1, yFrqz + 1);
				value += LinearInterpolation2D(v00, v01, v10, v11, x % (1 / frequency) * frequency, y % (1 / frequency) * frequency) * amplitude;

				frequency *= 2;
				amplitude *= persistence;

			}
			return value;

		}

		/// <summary>
		/// Cosine Interpolated 2D Value Noise
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="frequency"></param>
		/// <param name="amplitude"></param>
		/// <param name="persistance"></param>
		/// <param name="octaves"></param>
		/// <returns></returns>
		public static float ValueNoise2DCosine(int x, int y, float frequency, float amplitude, float persistence, float octaves) {
			float value = 0;
			frequency = 1 / frequency;
			float v00;
			float v01;
			float v10;
			float v11;
			int xFrqz, yFrqz;
			for (int i = 0; i < octaves; i++) {

				xFrqz = FastFloor(x * frequency);
				yFrqz = FastFloor(y * frequency);
				v00 = Noise2D(xFrqz, yFrqz);
				v01 = Noise2D(xFrqz, yFrqz + 1);
				v10 = Noise2D(xFrqz + 1, yFrqz);
				v11 = Noise2D(xFrqz + 1, yFrqz + 1);
				value += CosineInterpolation2D(v00, v01, v10, v11, x % (1 / frequency) * frequency, y % (1 / frequency) * frequency) * amplitude;
				frequency *= 2;
				amplitude *= persistence;
			}
			return value;
		}

		/// <summary>
		/// Cubic Interpolated 2D Value Noise
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="frequency"></param>
		/// <param name="amplitude"></param>
		/// <param name="persistance"></param>
		/// <param name="octaves"></param>
		/// <returns></returns>
		public static float ValueNoise2DCubic(int x, int y, float frequency, float amplitude, float persistence, float octaves) {
			float value = 0;
			frequency = 1 / frequency;
			int xFrqz, yFrqz;
			float[][] v = new float[4][];
			for (int i = 0; i < 4; i++)
				v[i] = new float[4];
			for (int i = 0; i < octaves; i++) {
				xFrqz = FastFloor(x * frequency);
				yFrqz = FastFloor(y * frequency);

				v[0][3] = Noise2D(xFrqz - 1, yFrqz + 2);
				v[0][2] = Noise2D(xFrqz, yFrqz + 2);
				v[0][1] = Noise2D(xFrqz + 1, yFrqz + 2);
				v[0][0] = Noise2D(xFrqz + 2, yFrqz + 2);

				v[3][3] = Noise2D(xFrqz - 1, yFrqz - 1);
				v[3][2] = Noise2D(xFrqz, yFrqz - 1);
				v[3][1] = Noise2D(xFrqz + 1, yFrqz - 1);
				v[3][0] = Noise2D(xFrqz + 2, yFrqz - 1);

				v[1][3] = Noise2D(xFrqz - 1, yFrqz + 1);
				v[1][2] = Noise2D(xFrqz, yFrqz + 1);
				v[1][1] = Noise2D(xFrqz + 1, yFrqz + 1);
				v[1][0] = Noise2D(xFrqz + 2, yFrqz + 1);

				v[2][3] = Noise2D(xFrqz - 1, yFrqz);
				v[2][2] = Noise2D(xFrqz, yFrqz);
				v[2][1] = Noise2D(xFrqz + 1, yFrqz);
				v[2][0] = Noise2D(xFrqz + 2, yFrqz);

				value += CubicInterpolation2D(v[0][3], v[0][2], v[0][1], v[0][0], v[1][3], v[1][2], v[1][1], v[1][0], v[2][3], v[2][2], v[2][1], v[2][0], v[3][3], v[3][2], v[3][1], v[3][0], x % (1 / frequency) * frequency, y % (1 / frequency) * frequency) * amplitude;
				frequency *= 2;
				amplitude *= persistence;
			}
			return value;
		}

		/// <summary>
		/// Linear Interpolated 3D Value Noise
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="z"></param>
		/// <param name="frequency"></param>
		/// <param name="amplitude"></param>
		/// <param name="persistance"></param>
		/// <param name="octaves"></param>
		/// <returns></returns>
		public static float ValueNoise3DLinear(int x, int y, int z, float frequency, float amplitude, float persistence, float octaves) {
			float value = 0;
			frequency = 1 / frequency;
			float v000;
			float v010;
			float v100;
			float v110;
			float v001;
			float v011;
			float v101;
			float v111;
			int xFrqz, yFrqz, zFrqz;
			for (int i = 0; i < octaves; i++) {
				xFrqz = FastFloor(x * frequency);
				yFrqz = FastFloor(y * frequency);
				zFrqz = FastFloor(z * frequency);
				v000 = Noise3D(xFrqz, yFrqz, zFrqz);
				v010 = Noise3D(xFrqz, yFrqz + 1, zFrqz);
				v100 = Noise3D(xFrqz + 1, yFrqz, zFrqz);
				v110 = Noise3D(xFrqz + 1, yFrqz + 1, zFrqz);
				v001 = Noise3D(xFrqz, yFrqz, zFrqz + 1);
				v011 = Noise3D(xFrqz, yFrqz + 1, zFrqz + 1);
				v101 = Noise3D(xFrqz + 1, yFrqz, zFrqz + 1);
				v111 = Noise3D(xFrqz + 1, yFrqz + 1, zFrqz + 1);
				value += LinearInterpolation3D(v000, v010, v100, v110, v001, v011, v101, v111,
					x % (1 / frequency) * frequency,
					y % (1 / frequency) * frequency,
					z % (1 / frequency) * frequency) * amplitude;
				frequency *= 2;
				amplitude *= persistence;
			}
			return value;
		}

		/// <summary>
		/// Cosine Interpolated 3D Value Noise
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="z"></param>
		/// <param name="frequency"></param>
		/// <param name="amplitude"></param>
		/// <param name="persistance"></param>
		/// <param name="octaves"></param>
		/// <returns></returns>
		public static float ValueNoise3DCosine(int x, int y, int z, float frequency, float amplitude, float persistence, float octaves) {
			float value = 0;
			float v000;
			float v010;
			float v100;
			float v110;
			float v001;
			float v011;
			float v101;
			float v111;
			frequency = 1 / frequency;
			int xFrqz, yFrqz, zFrqz;
			for (var i = 0; i < octaves; i++) {
				xFrqz = FastFloor(x * frequency);
				yFrqz = FastFloor(y * frequency);
				zFrqz = FastFloor(z * frequency);
				v000 = Noise3D(xFrqz, yFrqz, zFrqz);
				v010 = Noise3D(xFrqz, yFrqz + 1, zFrqz);
				v100 = Noise3D(xFrqz + 1, yFrqz, zFrqz);
				v110 = Noise3D(xFrqz + 1, yFrqz + 1, zFrqz);
				v001 = Noise3D(xFrqz, yFrqz, zFrqz + 1);
				v011 = Noise3D(xFrqz, yFrqz + 1, zFrqz + 1);
				v101 = Noise3D(xFrqz + 1, yFrqz, zFrqz + 1);
				v111 = Noise3D(xFrqz + 1, yFrqz + 1, zFrqz + 1);
				value += LinearInterpolation3D(v000, v010, v100, v110, v001, v011, v101, v111,
					x % (1 / frequency) * frequency,
					y % (1 / frequency) * frequency,
					z % (1 / frequency) * frequency) * amplitude;
				frequency *= 2;
				amplitude *= persistence;
			}
			return value;
		}

		/// <summary>
		/// Cubic Interpolated 3D Value Noise
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="z"></param>
		/// <param name="frequency"></param>
		/// <param name="amplitude"></param>
		/// <param name="persistance"></param>
		/// <param name="octaves"></param>
		/// <returns></returns>
		public static float ValueNoise3DCubic(int x, int y, int z, float frequency, float amplitude, float persistence, float octaves) {
			int xFrqz, yFrqz, zFrqz;
			float value = 0;
			frequency = 1 / frequency;
			float[][][] v = new float[4][][];
			for (int i = 0; i < 4; i++) {
				v[i] = new float[4][];
				for (int j = 0; j < 4; j++)
					v[i][j] = new float[4];
			}
			for (int i = 0; i < octaves; i++) {
				xFrqz = FastFloor(x * frequency);
				yFrqz = FastFloor(y * frequency);
				zFrqz = FastFloor(z * frequency);

				for (int vx = 0; vx < 4; vx++)
					for (int vy = 0; vy < 4; vy++)
						for (int vz = 0; vz < 4; vz++)
							v[vx][vy][vz] = Noise3D(xFrqz + vx - 1, yFrqz + vy - 1, zFrqz + vz - 1);

				value += CubicInterpolation3D(v, x % (1 / frequency) * frequency, y % (1 / frequency) * frequency, z % (1 / frequency) * frequency) * amplitude;
				frequency *= 2;
				amplitude *= persistence;
			}
			return value;
		}

		/// <summary>
		/// 1D Simplex Noise
		/// </summary>
		/// <param name="x"></param>
		/// <returns></returns>
		public static float SimplexNoise1D(float x, float frequency, float amplitude) {
			x *= frequency;

			int i0 = FastFloor(x);
			int i1 = i0 + 1;
			float x0 = x - i0;
			float x1 = x0 - 1.0f;

			float n0, n1;

			float t0 = 1.0f - x0 * x0;
			t0 *= t0;
			n0 = t0 * t0 * grad(perm[i0 & 0xff], x0);

			float t1 = 1.0f - x1 * x1;
			t1 *= t1;
			n1 = t1 * t1 * grad(perm[i1 & 0xff], x1);
			// The maximum value of this noise is 8*(3/4)^4 = 2.53125
			// A factor of 0.395 scales to fit exactly within [-1,1]
			return 0.395f * (n0 + n1) * amplitude;
		}

		/// <summary>
		/// 2D Simplex Noise
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		public static float SimplexNoise2D(float x, float y, float frequency, float amplitude) {
			x *= frequency;
			y *= frequency;

			const float F2 = 0.366025403f; // F2 = 0.5*(sqrt(3.0)-1.0)
			const float G2 = 0.211324865f; // G2 = (3.0-Math.sqrt(3.0))/6.0

			float n0, n1, n2; // Noise contributions from the three corners

			// Skew the input space to determine which simplex cell we're in
			float s = (x + y) * F2; // Hairy factor for 2D
			float xs = x + s;
			float ys = y + s;
			int i = FastFloor(xs);
			int j = FastFloor(ys);

			float t = (float)(i + j) * G2;
			float X0 = i - t; // Unskew the cell origin back to (x,y) space
			float Y0 = j - t;
			float x0 = x - X0; // The x,y distances from the cell origin
			float y0 = y - Y0;

			// For the 2D case, the simplex shape is an equilateral triangle.
			// Determine which simplex we are in.
			int i1, j1; // Offsets for second (middle) corner of simplex in (i,j) coords
			if (x0 > y0) { i1 = 1; j1 = 0; } // lower triangle, XY order: (0,0)->(1,0)->(1,1)
			else { i1 = 0; j1 = 1; }      // upper triangle, YX order: (0,0)->(0,1)->(1,1)

			// A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
			// a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
			// c = (3-sqrt(3))/6

			float x1 = x0 - i1 + G2; // Offsets for middle corner in (x,y) unskewed coords
			float y1 = y0 - j1 + G2;
			float x2 = x0 - 1.0f + 2.0f * G2; // Offsets for last corner in (x,y) unskewed coords
			float y2 = y0 - 1.0f + 2.0f * G2;

			// Wrap the integer indices at 256, to avoid indexing perm[] out of bounds
			int ii = i % 256;
			int jj = j % 256;

			// Calculate the contribution from the three corners
			float t0 = 0.5f - x0 * x0 - y0 * y0;
			if (t0 < 0.0f) n0 = 0.0f;
			else {
				t0 *= t0;
				n0 = t0 * t0 * grad(perm[ii + perm[jj]], x0, y0);
			}

			float t1 = 0.5f - x1 * x1 - y1 * y1;
			if (t1 < 0.0f) n1 = 0.0f;
			else {
				t1 *= t1;
				n1 = t1 * t1 * grad(perm[ii + i1 + perm[jj + j1]], x1, y1);
			}

			float t2 = 0.5f - x2 * x2 - y2 * y2;
			if (t2 < 0.0f) n2 = 0.0f;
			else {
				t2 *= t2;
				n2 = t2 * t2 * grad(perm[ii + 1 + perm[jj + 1]], x2, y2);
			}

			// Add contributions from each corner to get the final noise value.
			// The result is scaled to return values in the interval [-1,1].
			return 40.0f * (n0 + n1 + n2) * amplitude; // TODO: The scale factor is preliminary!
		}

		/// <summary>
		/// 3D Simplex Noise
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="z"></param>
		/// <returns></returns>
		public static float SimplexNoise3D(float x, float y, float z, float frequency, float amplitude) {
			x /= frequency;
			y /= frequency;
			z /= frequency;

			// Simple skewing factors for the 3D case
			const float F3 = 0.333333333f;
			const float G3 = 0.166666667f;

			float n0, n1, n2, n3; // Noise contributions from the four corners

			// Skew the input space to determine which simplex cell we're in
			float s = (x + y + z) * F3; // Very nice and simple skew factor for 3D
			float xs = x + s;
			float ys = y + s;
			float zs = z + s;
			int i = FastFloor(xs);
			int j = FastFloor(ys);
			int k = FastFloor(zs);

			float t = (float)(i + j + k) * G3;
			float X0 = i - t; // Unskew the cell origin back to (x,y,z) space
			float Y0 = j - t;
			float Z0 = k - t;
			float x0 = x - X0; // The x,y,z distances from the cell origin
			float y0 = y - Y0;
			float z0 = z - Z0;

			// For the 3D case, the simplex shape is a slightly irregular tetrahedron.
			// Determine which simplex we are in.
			int i1, j1, k1; // Offsets for second corner of simplex in (i,j,k) coords
			int i2, j2, k2; // Offsets for third corner of simplex in (i,j,k) coords

			/* This code would benefit from a backport from the GLSL version! */
			if (x0 >= y0) {
				if (y0 >= z0) { i1 = 1; j1 = 0; k1 = 0; i2 = 1; j2 = 1; k2 = 0; } // X Y Z order
				else if (x0 >= z0) { i1 = 1; j1 = 0; k1 = 0; i2 = 1; j2 = 0; k2 = 1; } // X Z Y order
				else { i1 = 0; j1 = 0; k1 = 1; i2 = 1; j2 = 0; k2 = 1; } // Z X Y order
			}
			else { // x0<y0
				if (y0 < z0) { i1 = 0; j1 = 0; k1 = 1; i2 = 0; j2 = 1; k2 = 1; } // Z Y X order
				else if (x0 < z0) { i1 = 0; j1 = 1; k1 = 0; i2 = 0; j2 = 1; k2 = 1; } // Y Z X order
				else { i1 = 0; j1 = 1; k1 = 0; i2 = 1; j2 = 1; k2 = 0; } // Y X Z order
			}

			// A step of (1,0,0) in (i,j,k) means a step of (1-c,-c,-c) in (x,y,z),
			// a step of (0,1,0) in (i,j,k) means a step of (-c,1-c,-c) in (x,y,z), and
			// a step of (0,0,1) in (i,j,k) means a step of (-c,-c,1-c) in (x,y,z), where
			// c = 1/6.

			float x1 = x0 - i1 + G3; // Offsets for second corner in (x,y,z) coords
			float y1 = y0 - j1 + G3;
			float z1 = z0 - k1 + G3;
			float x2 = x0 - i2 + 2.0f * G3; // Offsets for third corner in (x,y,z) coords
			float y2 = y0 - j2 + 2.0f * G3;
			float z2 = z0 - k2 + 2.0f * G3;
			float x3 = x0 - 1.0f + 3.0f * G3; // Offsets for last corner in (x,y,z) coords
			float y3 = y0 - 1.0f + 3.0f * G3;
			float z3 = z0 - 1.0f + 3.0f * G3;

			// Wrap the integer indices at 256, to avoid indexing perm[] out of bounds
			int ii = Mod(i, 256);
			int jj = Mod(j, 256);
			int kk = Mod(k, 256);

			// Calculate the contribution from the four corners
			float t0 = 0.6f - x0 * x0 - y0 * y0 - z0 * z0;
			if (t0 < 0.0f) n0 = 0.0f;
			else {
				t0 *= t0;
				n0 = t0 * t0 * grad(perm[ii + perm[jj + perm[kk]]], x0, y0, z0);
			}

			float t1 = 0.6f - x1 * x1 - y1 * y1 - z1 * z1;
			if (t1 < 0.0f) n1 = 0.0f;
			else {
				t1 *= t1;
				n1 = t1 * t1 * grad(perm[ii + i1 + perm[jj + j1 + perm[kk + k1]]], x1, y1, z1);
			}

			float t2 = 0.6f - x2 * x2 - y2 * y2 - z2 * z2;
			if (t2 < 0.0f) n2 = 0.0f;
			else {
				t2 *= t2;
				n2 = t2 * t2 * grad(perm[ii + i2 + perm[jj + j2 + perm[kk + k2]]], x2, y2, z2);
			}

			float t3 = 0.6f - x3 * x3 - y3 * y3 - z3 * z3;
			if (t3 < 0.0f) n3 = 0.0f;
			else {
				t3 *= t3;
				n3 = t3 * t3 * grad(perm[ii + 1 + perm[jj + 1 + perm[kk + 1]]], x3, y3, z3);
			}

			// Add contributions from each corner to get the final noise value.
			// The result is scaled to stay just inside [-1,1]
			return 32.0f * (n0 + n1 + n2 + n3) * amplitude; // TODO: The scale factor is preliminary!
		}

		private static byte[] perm = new byte[512] { 151,160,137,91,90,15,
			131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
			190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
			88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
			77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
			102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
			135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
			5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
			223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
			129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
			251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
			49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
			138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180,
			151,160,137,91,90,15,
			131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
			190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
			88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
			77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
			102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
			135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
			5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
			223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
			129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
			251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
			49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
			138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
		};

		private static int FastFloor(float x) {
			return (x > 0) ? ((int)x) : (((int)x) - 1);
		}

		private static int Mod(int x, int m) {
			int a = x % m;
			return a < 0 ? a + m : a;
		}

		private static float grad(int hash, float x) {
			int h = hash & 15;
			float grad = 1.0f + (h & 7);   // Gradient value 1.0, 2.0, ..., 8.0
			if ((h & 8) != 0) grad = -grad;         // Set a random sign for the gradient
			return (grad * x);           // Multiply the gradient with the distance
		}

		private static float grad(int hash, float x, float y) {
			int h = hash & 7;      // Convert low 3 bits of hash code
			float u = h < 4 ? x : y;  // into 8 simple gradient directions,
			float v = h < 4 ? y : x;  // and compute the dot product with (x,y).
			return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -2.0f * v : 2.0f * v);
		}

		private static float grad(int hash, float x, float y, float z) {
			int h = hash & 15;     // Convert low 4 bits of hash code into 12 simple
			float u = h < 8 ? x : y; // gradient directions, and compute dot product.
			float v = h < 4 ? y : h == 12 || h == 14 ? x : z; // Fix repeats at h = 12 to 15
			return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -v : v);
		}

		private static float grad(int hash, float x, float y, float z, float t) {
			int h = hash & 31;      // Convert low 5 bits of hash code into 32 simple
			float u = h < 24 ? x : y; // gradient directions, and compute dot product.
			float v = h < 16 ? y : z;
			float w = h < 8 ? z : t;
			return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -v : v) + ((h & 4) != 0 ? -w : w);
		}

	}
}
