﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace DungeonSoft.HelpfulScripts.Unity {
	public static class UnityParallel {

		private static ParallelTask[] threads;

		private static readonly object LockObject = new object();

		public static void Load(int threadAmount = 8) {
			lock (LockObject) {
				if (threads != null) return;

				threads = new ParallelTask[threadAmount];
				for (var i = 0; i < threadAmount; i++) {
					var task = new ParallelTask();
					threads[i] = task;
					task.thread = new Thread(task.Execute);
				}

				foreach (ParallelTask task in threads) task.thread.Start();
			}
		}

		public static void Unload() {
			lock (LockObject) {
				foreach (ParallelTask t in threads) t.stopThread = true;
				threads = null;
			}
		}

		private class ParallelTask {
			public Thread thread;
			public readonly Queue<Action> actions = new Queue<Action>();
			public bool allowExecution;
			public bool stopThread;

			public void Execute() {
				while (!stopThread) {
					while (!allowExecution) Thread.Sleep(1);
					while (actions.Count > 0) actions.Dequeue()();
					allowExecution = false;
				}
			}

		}

		public static void ForEach<T>(IEnumerable<T> source, Action<T> action) {
			lock (LockObject) {
				Load();

				var i = 0;
				foreach (T s in source) {
					T s1 = s;
					AddTask(ref i, () => action(s1));
				}

				StartExecution();
			}

		}

		public static void Do(IEnumerable<Action> actions) {
			lock (LockObject) {
				Load();

				var i = 0;
				foreach (Action a in actions) {
					Action s1 = a;
					AddTask(ref i, s1);
				}

				StartExecution();
			}
		}

		private static void AddTask(ref int i, Action action) {
			ParallelTask task = threads[i++ % threads.Length];
			task.allowExecution = false;
			task.actions.Enqueue(action);
		}

		private static void StartExecution() {
			foreach (ParallelTask task in threads) task.allowExecution = true;

			while (threads.Any(x => x.allowExecution)) ;
		}

	}
}