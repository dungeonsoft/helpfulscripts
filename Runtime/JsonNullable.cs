﻿
using System.Collections.Generic;

namespace DungeonSoft.HelpfulScripts {
	public struct JsonNullable<T> where T : struct {
		public bool Equals(JsonNullable<T> other) {
			return isNull == other.isNull && EqualityComparer<T>.Default.Equals(value, other.value);
		}

		public override bool Equals(object obj) {
			if (ReferenceEquals(null, obj)) return false;
			return obj is JsonNullable<T> && Equals((JsonNullable<T>)obj);
		}

		public override int GetHashCode() {
			unchecked {
				return (value.GetHashCode() * 397) ^ EqualityComparer<T>.Default.GetHashCode(value);
			}
		}

		public bool isNull;
		public T value;

		public static bool operator ==(JsonNullable<T> left, object right) {
			return right == null ? left.isNull : Equals(left, right);
		}

		public static bool operator !=(JsonNullable<T> left, object right) {
			return !(left == right);
		}

		public static implicit operator JsonNullable<T>(T? t) {
			return new JsonNullable<T> { value = t ?? default(T), isNull = t == null };
		}

		public static implicit operator JsonNullable<T>(T t) {
			return new JsonNullable<T> { value = t };
		}

		public static implicit operator T(JsonNullable<T> t) {
			return t.value;
		}

	}
}
