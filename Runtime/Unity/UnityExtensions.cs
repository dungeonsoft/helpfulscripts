﻿using UnityEngine;

namespace DungeonSoft.HelpfulScripts.Unity {
	public static class UnityExtensions {
		public static Sprite ToSprite(this Texture2D texture) {
			return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100f);
		}
		public static T Instantiate<T>(this T obj) where T : Object {
			return Object.Instantiate(obj);
		}

	}
}