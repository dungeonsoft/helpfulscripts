﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DungeonSoft.HelpfulScripts {
	/// <summary>
	/// Returns random values in a circular manner
	/// </summary>
	public sealed class BetterRandom<T> {

		private readonly Random rand;

		private readonly IList<T> values;
		private int current;

		public BetterRandom(IList<T> values, int? seed = null) {
			rand = seed == null ? new Random() : new Random((int)seed);
			this.values = values;
			Shuffle();
		}

		public T GetNext() {
			if (values.Count == 0) return default(T);
			if (values.Count == 1) return values[0];

			T t = values[current++];
			if (current != values.Count) return t;
			Shuffle();
			current = 0;

			if (!Equals(values.First(), t)) return t;
			T h = values[0];
			values[0] = values[values.Count - 1];
			values[values.Count - 1] = h;

			return t;
		}

		private void Shuffle() {
			int n = values.Count;
			while (n > 1) {
				n--;
				int k = rand.Next(n + 1);
				T value = values[k];
				values[k] = values[n];
				values[n] = value;
			}
		}

		public int Count => values.Count;
	}
}
