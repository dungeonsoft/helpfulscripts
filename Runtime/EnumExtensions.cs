﻿using System;
using System.Linq;

namespace DungeonSoft.HelpfulScripts {
	public static class EnumExtensions {
		public static T GetEnumAttribute<T>(this Enum enumValue) where T : Attribute {
			T attribute = enumValue.GetType().GetField(enumValue.ToString()).GetCustomAttributes<T>().FirstOrDefault();
			return attribute;
		}
	}
}