﻿using System.IO;
using System.Xml.Linq;
using UnityEngine;

namespace DungeonSoft.HelpfulScripts.Unity {
	public static class SerializationExtensions {
		public static T DeserializeFromXml<T>(this TextAsset textAsset) {
			return textAsset.text.DeserializeFromXml<T>();
		}

		public static XDocument DeserializeFromXDoc(this TextAsset textAsset) {
			return textAsset.text.DeserializeFromXDoc();
		}

	}
}
