﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DungeonSoft.HelpfulScripts {
	public abstract class KeyCurve<T> {

		public enum LoopMode {
			Clamped,
			Looped,
			PingPong
		}

		public LoopMode loopMode = LoopMode.Clamped;

		public struct Key<T1> {
			public float time;
			public T1 value;
		}

		public List<Key<T>> keys = new();

		private static float GetT(float t, LoopMode loopMode) {
			switch (loopMode) {
				case LoopMode.Clamped:
					return Math.Clamp(t, 0f, 1f);
				case LoopMode.Looped:
					if (t < 0) t += (int)(-t) + 1;
					return t % 1.0f;
				case LoopMode.PingPong:
					return Math.Abs(((t + 1f) % (1f * 2)) - 1f);
				//return Mathf.PingPong(t, 1f);
				default:
					return t;
			}
		}

		//public Easings.Functions ease = Easings.Functions.Linear;

		private static float InverseLerp(float a, float b, float t) {
			return (t - a) / (b - a);
		}

		public T Evaluate(float time) {
			float min = keys.Min(x => x.time);
			float max = keys.Max(x => x.time);
			float length = max - min;

			float inverse = InverseLerp(min, max, time);
			time = min + length * GetT(inverse, loopMode);
			//time = Easings.Interpolate(time, ease);

			List<Key<T>> ordered = keys;//.OrderBy(x => x.time).ToList();
			for (var i = 0; i < ordered.Count - 1; i++) {
				Key<T> start = ordered[i];
				Key<T> end = ordered[i + 1];
				if (time > end.time) continue;
				float t = InverseLerp(start.time, end.time, time);
				return Lerp(start.value, end.value, t);
			}

			throw new Exception("KeyCurve evaluation overflow!");

		}

		public float TotalLength {
			get { return keys.Max(x => x.time); }
		}

		protected abstract T Lerp(T a, T b, float t);

	}

	public class KeyCurveFloat : KeyCurve<float> {
		protected override float Lerp(float a, float b, float t) {
			return a * (1.0f - t) + (b * t);
		}
	}

}
