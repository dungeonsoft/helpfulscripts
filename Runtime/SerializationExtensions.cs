﻿using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace DungeonSoft.HelpfulScripts {
	public static class SerializationExtensions {

		public static T DeserializeFromXml<T>(this string data) {
			var serializer = new XmlSerializer(typeof(T));
			T t;
			using TextReader reader = new StringReader(data);
			return (T)serializer.Deserialize(reader);
		}

		public static XDocument DeserializeFromXDoc(this string data) {
			using TextReader reader = new StringReader(data);
			return XDocument.Load(reader);
		}

		/*
		public static T DeserializeFromJson<T>(this string data) {
			return JsonConvert.DeserializeObject<T>(data);
		} */

	}
}
