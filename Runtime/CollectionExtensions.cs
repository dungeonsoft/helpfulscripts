﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace DungeonSoft.HelpfulScripts {
	public static class CollectionExtensions {
		public static List<T> CombineArrays<T>(this List<T> first, params IEnumerable<T>[] arrays) {
			var returnList = new List<T>(first);
			returnList.AddRange(arrays.SelectMany(array => array));
			return returnList;
		}

		public static void Swap(this IList collection, int a, int b) {
			object helper = collection[a];
			collection[a] = collection[b];
			collection[b] = helper;
		}

		public static void Split<T>(this T[] array, int index, out T[] first, out T[] second) {
			first = array.Take(index).ToArray();
			second = array.Skip(index).ToArray();
		}

		public static void SplitMidPoint<T>(this T[] array, out T[] first, out T[] second) {
			Split(array, array.Length / 2, out first, out second);
		}

		public static IEnumerable<IList<T>> Split<T>(this IList<T> array, int numberOfSplits) {
			//var collections = new IList<T>[numberOfSplits];
			if (numberOfSplits < 1) numberOfSplits = 1;
			//yield return array;
			//yield break;
			for (var i = 0; i < numberOfSplits; i++) {
				int start = i * (array.Count / numberOfSplits);
				int last = i == numberOfSplits - 1 ? array.Count : (i + 1) * (array.Count / numberOfSplits);
				var list = new List<T>();
				for (int j = start; j < last; j++) list.Add(array[j]);
				yield return list;
			}
		}

		public static LinkedListNode<T> Add<T>(this LinkedList<T> list, T entry) {
			return list.AddLast(entry);
		}

		public static ReadOnlyCollection<T> ToReadOnlyCollection<T>(this IList<T> collection) {
			return new(collection);
		}

		public static IEnumerable<T> ToIEnumerable<T>(this Array array) {
			foreach (object t in array) yield return (T)t;
		}

		public static IEnumerable<T> EveryNthElement<T>(this IEnumerable<T> collection, int n) {
			var current = 0;
			foreach (T item in collection)
				if (current++ % n == 0)
					yield return item;
		}

		public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector) {
			return source.MinBy(selector, null);
		}

		public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source,
			Func<TSource, TKey> selector, IComparer<TKey> comparer) {
			if (source == null) throw new ArgumentNullException(nameof(source));
			if (selector == null) throw new ArgumentNullException(nameof(selector));
			comparer ??= Comparer<TKey>.Default;

			using IEnumerator<TSource> sourceIterator = source.GetEnumerator();
			if (!sourceIterator.MoveNext()) throw new InvalidOperationException("Sequence contains no elements");
			TSource min = sourceIterator.Current;
			TKey minKey = selector(min);
			while (sourceIterator.MoveNext()) {
				TSource candidate = sourceIterator.Current;
				TKey candidateProjected = selector(candidate);
				if (comparer.Compare(candidateProjected, minKey) < 0) {
					min = candidate;
					minKey = candidateProjected;
				}
			}

			return min;
		}

		private static readonly Random Rand = new Random();

		public static T Random<T>(this IList<T> array) {
			return array[Rand.Next(array.Count)];
		}

		public static T Random<T>(this IList<T> array, int seed) {
			return array[PyreNoise.Range(0, array.Count, seed)];
		}

		public static T Random<T>(this IList<T> array, ref int seed) {
			return array[PyreNoise.Range(0, array.Count, ref seed)];
		}

		public static IList<T> Shuffled<T>(this IEnumerable<T> source) {
			return source.Shuffled(Rand.Next(10000));
		}

		public static IList<T> Shuffled<T>(this IEnumerable<T> source, int seed) {
			var collection = new List<T>(source);
			collection.Shuffle(seed);
			return collection;
		}

		public static void Shuffle<T>(this IList<T> list) {
			list.Shuffle(Rand.Next(10000));
		}

		public static void Shuffle<T>(this IList<T> list, int seed) {
			int n = list.Count;
			while (n > 1) {
				n--;
				int k = PyreNoise.Range(0, n + 1, ref seed);
				T value = list[k];
				list[k] = list[n];
				list[n] = value;
			}

			/*
			 // OLD
			T[] elements = source.ToArray();
			// Note i > 0 to avoid final pointless iteration
			for (int i = elements.Length - 1; i > 0; i--) {
				// Swap element "i" with a random earlier element it (or itself)
				int swapIndex = PyreNoise.Range(0, i + 1, ref seed); //rng.Next(i + 1);
				yield return elements[swapIndex];
				elements[swapIndex] = elements[i];
				// we don't actually perform the swap, we can forget about the
				// swapped element because we already returned it.
			}

			// there is one item remaining that was not returned - we return it now
			yield return elements[0]; */
		}

	}
}