﻿using System.Collections.Generic;
using UnityEngine;

namespace DungeonSoft.HelpfulScripts.Unity {
	public static class RectExtensions {

		public static IEnumerable<Vector2> Corners(this Rect rect) {
			yield return new Vector2(rect.xMin, rect.yMin);
			yield return new Vector2(rect.xMin, rect.yMax);
			yield return new Vector2(rect.xMax, rect.yMax);
			yield return new Vector2(rect.xMax, rect.yMin);
		}

	}
}
