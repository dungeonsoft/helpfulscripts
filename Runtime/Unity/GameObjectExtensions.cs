﻿using UnityEngine;

namespace DungeonSoft.HelpfulScripts.Unity {
	public static class GameObjectExtensions {
		public static void Destroy(this GameObject gameObject) {
			Object.Destroy(gameObject);
		}

		public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component {
			var t = gameObject.GetComponent<T>();
			if (t == null) t = gameObject.AddComponent<T>();
			return t;
		}

		public static T GetOrAddComponent<T>(this Component component) where T : Component {
			return component.gameObject.GetOrAddComponent<T>();
		}
	}
}
